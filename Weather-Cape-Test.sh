#!/bin/bash

cape="BB-BONE-WTHR-01"
lux1=/sys/bus/i2c/devices/1-0039/lux1_input
humidity1=/sys/bus/i2c/devices/1-0040/humidity1_input
temp1=/sys/bus/i2c/devices/1-0040/temp1_input
temp0=/sys/bus/i2c/devices/1-0077/temp0_input
pressure0=/sys/bus/i2c/devices/1-0077/pressure0_input
PRESSURE_VALUE="INVALID"
LUX_VALUE="INVALID"
TEMP0_VALUE="INVALID"
TEMP1_VALUE="INVALID"
HUMIDITY_VALUE="INVALID"


function setup {

	grep 2>&1 >/dev/null "$cape" /sys/devices/bone_capemgr*/slots
	if [ $? -ne 0 ] ; then
		echo "$cape:00B0" >/sys/devices/bone_capemgr*/slots
		sleep 5
		# check if loaded
		grep 2>&1 >/dev/null "$cape" /sys/devices/bone_capemgr*/slots
		if [ $? -ne 0 ] ; then
			echo -e "Failed to load $cape\r"
			exit 5
		fi
	fi

}


function get_lux {

	if [ -e $lux1 ]; then
		LUX_VALUE=`cat "$lux1"`
	else
		echo -e "Invalid lux value\r"
	fi		
	return 0

}


function get_humidity {

	if [ -e $humidity1 ]; then
		HUMIDITY_VALUE=$(expr `cat "$humidity1"` / 1000)
	else
		echo -e "Invalid humidity value\r"
	fi	
        return 0

}


function get_temp1 {

        if [ -e $temp1 ]; then
		TEMP1_VALUE=$(expr `cat "$temp1"` / 1000)
	else
		echo -e "Invalid temperature data\r"
	fi
        return 0

}


function get_temp0 {

        if [ -e $temp0 ]; then
		TEMP0_VALUE=$(expr `cat "$temp0"` / 10)
	else	
		echo -e "Invalid temperature data\r"
        fi
	return 0

}


function get_pressure {

	if [ -e $pressure0 ]; then
		PRESSURE_VALUE=$(expr `cat "$pressure0"` / 100)
	else
		echo -e "Invalid pressure data\r"
        fi
	return 0

}


function read_data {

	#echo -e "Reading sensor data...\r"
	echo -e "\r"
	echo -e "U5 (TSL2550)'s illuminance\t= $LUX_VALUE lux\t(range: 300 - 700)\r"
	echo -e "U3 (BMP085)'s temperature\t= $TEMP0_VALUE degree C\t(range: 15 - 37)\r"
	echo -e "U3 (BMP085)'s pressure\t\t= $PRESSURE_VALUE millibar\t(range: 900-1100)\r"
	echo -e "U2 (HTU21)'s humidity\t\t= $HUMIDITY_VALUE %\t\t(range: 10 - 90)\r"
	echo -e "U2 (HTU21)'s temperature\t= $TEMP1_VALUE degree C\t(range: 15 - 37)\r"

}


# start setting up
clear
echo -e "Start testing Weather Cape rev B\r"
echo -e "\r"
setup

exit_code=0 # 0: looping, 1: exit

while [ $exit_code -ne 3 ]; do
	echo -e "Reading sensor data...\r"
	# get sensor values
	get_lux
	if [ $? -ne 0 ] ; then
		echo -e "Error when getting U5 (TSL2550) value\r"
		break
	fi
	get_temp1
	if [ $? -ne 0 ] ; then
                echo -e "Error when getting U2 (HTU21) value\r"
                break
        fi
	get_temp0
	if [ $? -ne 0 ] ; then
                echo -e "Error when getting U3 (BMP085) value\r"
                break
        fi
	get_pressure
	if [ $? -ne 0 ] ; then
                echo -e "Error when getting U3 (BMP085) value\r"
                break
        fi
	get_humidity
	if [ $? -ne 0 ] ; then
                echo -e "Error when getting U2 (HTU21) value\r"
                break
        fi

	# read data from sensors
	read_data
	# echo -e "\r"
	
	# echo -n "Exit? Press y to exit or press n to continue: "
	# read text
	# echo "Read command $?"
	# echo -n "Do you want to exit (y/n)? "
	#while true; do
  	#	echo -n "Do you want to exit (y/n)? "
	#	read answer </dev/ttyO0
	#	if [[ $answer = [Yy] ]]; then
    	#		exit_code=1
    	#		break
	#	elif [[ $answer = [Nn] ]]; then
	#		exit_code=0
	#		break
  	#	fi
	#	echo -e "\r"
	#	# echo -n "Do you want to exit (y/n)? "
	#done  

	#while true ; do
	#	echo -n "Exit? Press y to exit or press n to continue: "
        #	read -r text
	#	echo "Read command $?"
	#	# echo -e "\r"
	#	echo "Text enter: $text"
	#	# echo -e "\r"
	#	if [ "$text" == "y" ] ; then
	#		echo "Test ended."
	#		exit_code=1
	#		break
	#	#elif [ "X$text" = "X" ] ; then
	#	elif [ "$text" == "n" ] ; then
	#		exit_code=0
	#		break				
	#	fi
		# echo -n "Exit? Press y to exit or press n to continue: "
	#done
	
	#echo "Exit code: $exit_code"
	echo -e "\r" 
	echo -e "\r"
	echo -e "\r"
	exit_code=`expr $exit_code + 1`
	#if [ $exit_code -eq 1 ] ; then
	#	break		
	#fi
done


